import { WebPlugin } from '@capacitor/core';
import { VoipPlugin } from './definitions';

export class VoipWeb extends WebPlugin implements VoipPlugin {
  constructor() {
    super({
      name: 'Voip',
      platforms: ['web']
    });
  }

  async echo(options: { value: string }): Promise<{ value: string }> {
    console.log('ECHO', options);
    return options;
  }

  async startCall(options: { sender: string, uuid: string, companyId: string, branchId: string, jid: string, hasVideo: boolean }): Promise<{ success: boolean }> {
    console.log('startCall', options);
    return { success: true };
  }

  async cancelCall(options: { uuid: string }): Promise<{ success: boolean }> {
    console.log('cancelCall', options);
    return { success: true };
  }
}

const Voip = new VoipWeb();

export { Voip };

import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(Voip);
