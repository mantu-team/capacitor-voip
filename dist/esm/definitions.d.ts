import { PluginListenerHandle } from '@capacitor/core';
export interface RegistrationToken {
    token: string;
}
export interface VoipNotification {
    data: any;
}
declare module '@capacitor/core' {
    interface PluginRegistry {
        Voip: VoipPlugin;
    }
}
export interface VoipPlugin {
    echo(options: {
        value: string;
    }): Promise<{
        value: string;
    }>;
    startCall(options: {
        sender: string;
        uuid: string;
        companyId: string;
        branchId: string;
        jid: string;
        hasVideo: boolean;
    }): Promise<{
        success: boolean;
    }>;
    cancelCall(options: {
        uuid: string;
    }): Promise<{
        success: boolean;
    }>;
    addListener(eventName: 'registration', listenerFunc: (res: RegistrationToken) => void): PluginListenerHandle;
    addListener(eventName: 'registrationError', listenerFunc: () => void): PluginListenerHandle;
    addListener(eventName: 'pushNotificationReceived', listenerFunc: (notification: VoipNotification) => void): PluginListenerHandle;
}
