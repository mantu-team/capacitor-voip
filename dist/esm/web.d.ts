import { WebPlugin } from '@capacitor/core';
import { VoipPlugin } from './definitions';
export declare class VoipWeb extends WebPlugin implements VoipPlugin {
    constructor();
    echo(options: {
        value: string;
    }): Promise<{
        value: string;
    }>;
    startCall(options: {
        sender: string;
        uuid: string;
        companyId: string;
        branchId: string;
        jid: string;
        hasVideo: boolean;
    }): Promise<{
        success: boolean;
    }>;
    cancelCall(options: {
        sender: string;
        uuid: string;
        companyId: string;
        branchId: string;
        jid: string;
        hasVideo: boolean;
    }): Promise<{
        success: boolean;
    }>;
}
declare const Voip: VoipWeb;
export { Voip };
