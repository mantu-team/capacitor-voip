import Foundation
import Capacitor
import PushKit
import CallKit
import AVFoundation

/**
 * Please read the Capacitor iOS Plugin Development Guide
 * here: https://capacitor.ionicframework.com/docs/plugins/ios
 */
@objc(Voip)
public class Voip: CAPPlugin, PKPushRegistryDelegate, CXProviderDelegate {
    
    private static let UpdatePushCredentialsNotificationName = "updatePushKitCredentialsNotification"
    private static let InvalidatePushCredentialsNotificationName = "invalidatePushKitCredentialsNotification"
    private static let IncomingPushNotificationName = "incomingPushKitPushNotification"
    private static let OnCallActionName = "onCallAction"
    
    @objc private var callProvider: CXProvider?
    let callManager = CallManager()
    
    override public func load() {
        
        super.load()
        
        registerForVOIPNotifications()
        initCXComponents()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.pushRegistryUpdateCredentials(notification:)), name: Notification.Name(Voip.UpdatePushCredentialsNotificationName), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.pushRegistryInvalidCredentials(notification:)), name: Notification.Name(Voip.InvalidatePushCredentialsNotificationName), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.pushRegistryIncomingPush(notification:)), name: Notification.Name(Voip.IncomingPushNotificationName), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onCallAction(callAction:)), name: Notification.Name(Voip.OnCallActionName), object: nil)
    }
    
    @objc func echo(_ call: CAPPluginCall) {
        let value = call.getString("value") ?? ""
        call.success([
            "value": value
        ])
    }
    
    @objc func startCall(_ call: CAPPluginCall) {
        
        if let sender = call.getString("sender"),
           let uuidString = call.getString("uuid"),
           let companyId = call.getString("companyId"),
           let branchId = call.getString("branchId"),
           let jid = call.getString("jid"),
           let hasVideo = call.getBool("hasVideo"),
           let callUUID = UUID(uuidString: uuidString) {
            
            let outgoingCall = Call(uuid: callUUID, outgoing: true, handle: jid,sender:sender, companyId: companyId, branchId: branchId, videoCall: hasVideo)
            self.callManager.add(call: outgoingCall)
            self.callManager.startCall(handle: jid, videoEnabled: hasVideo)
            
            call.success([
                "success": true
            ])
        } else {
            call.success([
                "success": false
            ])
        }
    }
    
    @objc func cancelCall(_ call: CAPPluginCall) {
        
        if let uuidString = call.getString("uuid"),
           let callUUID = UUID(uuidString: uuidString) {

            guard let callEntity = callManager.callWithUUID(uuid: callUUID) else {
                call.success([
                "success": false
                ])
                return
            }

            callEntity.end()            
            callManager.end(call: callEntity)
            
            call.success([
                "success": true
            ])
        } else {
            call.success([
                "success": false
            ])
        }
    
    }
    
    @objc public func registerForVOIPNotifications() {
        let voipRegistry = PKPushRegistry(queue: DispatchQueue.main)
        voipRegistry.delegate = self
        voipRegistry.desiredPushTypes = [PKPushType.voIP]
    }
    
    @objc public func initCXComponents() {
        
        let config = CXProviderConfiguration(localizedName: "Mantu")
        config.supportsVideo = true
        config.supportedHandleTypes = [.phoneNumber]
        config.maximumCallsPerCallGroup = 1
        
        // Create the provider and attach the custom delegate object
        // used by the app to respond to updates.
        callProvider = CXProvider(configuration: config)
        callProvider?.setDelegate(self, queue: DispatchQueue.main)
        
    }
    
    @objc public func pushRegistryUpdateCredentials(notification: NSNotification){
        guard let deviceToken = notification.object as? String else {
            return
        }
        notifyListeners("registration", data: [
            "token": deviceToken
        ], retainUntilConsumed: true)
    }
    
    @objc public func pushRegistryInvalidCredentials(notification: NSNotification) {
        NSLog("Voip Plugin - Error registering device");
        notifyListeners("registrationError", data: [:], retainUntilConsumed: true)
    }
    
    @objc public func onCallAction(callAction: CXCallAction) {
        NSLog("Voip Plugin - On CXCallAction");
        notifyListeners("onCallAction", data: [
            "type": type(of: callAction)
        ], retainUntilConsumed: true)
    }
    
    @objc public func pushRegistryIncomingPush(notification: NSNotification) {
        guard let payload = notification.object as? [AnyHashable : Any] else {
            return
        }
        NSLog("Voip Plugin - ", notification)
        notifyListeners("pushNotificationReceived", data: [
            "data": payload
        ], retainUntilConsumed: true)
    }
    
    public func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        if (pushCredentials.token.count == 0) {
            NSLog("Voip Plugin - No device token");
            return;
        }
        NSLog("Voip Pl ugin - Device registered successfully");
        let token = pushCredentials.token.map { String(format: "%.2hhx", $0) }.joined()
        NSLog("Voip Plugin - Token: ", token);
        NotificationCenter.default.post(name: Notification.Name(Voip.UpdatePushCredentialsNotificationName), object: token)
    }
    
    public func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
        NSLog("Voip Plugin - VOIP notification received");
        if type == .voIP {
            // Extract the call information from the push notification payload
            if let sender = payload.dictionaryPayload["sender"] as? String,
               let uuidString = payload.dictionaryPayload["callUUID"] as? String,
               let companyId = payload.dictionaryPayload["companyId"] as? String,
               let branchId = payload.dictionaryPayload["branchId"] as? String,
               let jid = payload.dictionaryPayload["jid"] as? String,
               let hasVideo = payload.dictionaryPayload["hasVideo"] as? Bool,
               let callUUID = UUID(uuidString: uuidString) {
                
                NSLog("Voip Plugin - Passed checks");
                // Configure the call information data structures.
                let callUpdate = CXCallUpdate()
                callUpdate.remoteHandle = CXHandle(type: .generic, value: jid)
                callUpdate.hasVideo = hasVideo
                callUpdate.localizedCallerName = sender
                
                let backgroundTaskIdentifier =
                    UIApplication.shared.beginBackgroundTask(expirationHandler: nil)
                
                NSLog("Voip Plugin - Reporting New Incoming Call");
                self.callProvider?.reportNewIncomingCall(with: callUUID, update: callUpdate) { error in
                    if error == nil {
                        let call = Call(uuid: callUUID, handle: callUpdate.remoteHandle!.value, sender:sender, companyId: companyId, branchId: branchId, videoCall: hasVideo)
                        self.callManager.add(call: call)
                    }
                    UIApplication.shared.endBackgroundTask(backgroundTaskIdentifier)
                    completion()
                }
                
                NotificationCenter.default.post(name: Notification.Name(Voip.IncomingPushNotificationName), object: payload.dictionaryPayload)
                
            }
        }
        
    }
    
    func handlePushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        NSLog("Voip Plugin - Error registering device");
        NotificationCenter.default.post(name: Notification.Name(Voip.InvalidatePushCredentialsNotificationName), object: nil)
    }
    
    @objc public func providerDidReset(_ provider: CXProvider) {
        //    stopAudio()
        
        for call in callManager.calls {
            call.end()
        }
        
        callManager.removeAllCalls()
    }
    
    @objc public func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        guard let call = callManager.callWithUUID(uuid: action.callUUID) else {
            action.fail()
            return
        }
        call.answer()
        action.fulfill()
    }
    
    @objc public func provider(_ provider: CXProvider, didDeactivate audioSession: AVAudioSession) {
        print("HAIM DEBUG didDeactivate audioSession: AVAudioSession")
    }
    
    @objc public func provider(_ provider: CXProvider, didActivate audioSession: AVAudioSession) {
        print("HAIM DEBUG didActivate audioSession: AVAudioSession")
        do {
                        
            guard let call = callManager.getActiveCall() else {
                return
            }
            
            provider.reportCall(with: call.uuid, endedAt: nil, reason: .answeredElsewhere)
            call.end()
            
            let queryItems = [
                URLQueryItem(name: "companyId", value: call.companyId),
                URLQueryItem(name: "branchId", value: call.branchId),
                URLQueryItem(name: "uuid", value: call.uuid.uuidString),
                URLQueryItem(name: "jid", value: call.handle),
                URLQueryItem(name: "videoCall", value: String(call.videoCall)),
                URLQueryItem(name: "action", value: "answer")
            ]
            var urlComps = URLComponents(string: "voip.mantu.ionic")!
            urlComps.queryItems = queryItems

            NSLog("CAPBridge.handleOpenUrl %@", [urlComps.url!])
            
            CAPBridge.handleOpenUrl(urlComps.url!, [:])

        }
        catch let error as NSError {
            print("Error: Could not setActive to false: \(error), \(error.userInfo)")
        }
    }
    
    @objc public func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        guard let call = callManager.callWithUUID(uuid: action.callUUID) else {
            action.fail()
            return
        }
        
        if call.state != .ended {

            let callAction = call.state == .active ? "end" : "decline"
        
            let queryItems = [
                URLQueryItem(name: "companyId", value: call.companyId),
                URLQueryItem(name: "branchId", value: call.branchId),
                URLQueryItem(name: "uuid", value: call.uuid.uuidString),
                URLQueryItem(name: "jid", value: call.handle),
                URLQueryItem(name: "videoCall", value: String(call.videoCall)),
                URLQueryItem(name: "action", value: callAction)
            ]
            var urlComps = URLComponents(string: "voip.mantu.ionic")!
            urlComps.queryItems = queryItems
            
            CAPBridge.handleOpenUrl(urlComps.url!, [:])
        }
        
        call.end()
        callManager.remove(call: call)
        
        action.fulfill()
        
    }
    
    @objc public func provider(_ provider: CXProvider, perform action: CXSetHeldCallAction) {
        guard let call = callManager.callWithUUID(uuid: action.callUUID) else {
            action.fail()
            return
        }
        
        call.state = action.isOnHold ? .held : .active
        
        if call.state == .held {
            //      stopAudio()
        } else {
            //      startAudio()
        }
        
        action.fulfill()
    }
    
    @objc public func provider(_ provider: CXProvider, perform action: CXStartCallAction) {
        
        guard let call = callManager.callWithUUID(uuid: action.callUUID) else {
            action.fail()
            return
        }
        
        //    configureAudioSession()
        
        call.connectedStateChanged = { [weak self, weak call] in
            guard
                let self = self,
                let call = call
            else {
                return
            }
            
            if call.connectedState == .pending {
                self.callProvider?.reportOutgoingCall(with: call.uuid, startedConnectingAt: nil)
            } else if call.connectedState == .complete {
                self.callProvider?.reportOutgoingCall(with: call.uuid, connectedAt: nil)
            }
        }
        
        call.start { [weak self, weak call] success in
            if success {
                action.fulfill()
            } else {
                action.fail()
            }
        }
    }
}

